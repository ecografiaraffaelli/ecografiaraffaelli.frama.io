---
title: "Galleria"
permalink: /galleria/
layout: single
author_profile: true
header:
  image: /assets/images/process_scrivania_bandeaux.jpg
gallery:
  - url: /assets/images/targa_dott_g_raffaelli.jpg
    image_path: /assets/images/targa_dott_g_raffaelli_th.jpg
    alt: "Targa Dott. G. Raffaelli"
    title: "Targa Dott. G. Raffaelli"
  - url: /assets/images/eco_trasp1.jpg
    image_path: /assets/images/eco_trasp1_th.jpg
    alt: "Ecografo da trasporto 1"
    title: "Ecografo da trasporto 1"
  - url: /assets/images/eco_trasp2.jpg
    image_path: /assets/images/eco_trasp2_th.jpg
    alt: "Ecografo da trasporto 2"
    title: "Ecografo da trasporto 2"
  - url: /assets/images/process_ambulatorio_1.jpg
    image_path: /assets/images/process_ambulatorio_1_th.jpg
    alt: "Ambulatorio completto"
    title: "Ambulatorio completto"
  - url: /assets/images/process_lettino.jpg
    image_path: /assets/images/process_lettino_th.jpg
    alt: "Lettino e ecografo"
    title: "Lettino e ecografo"
  - url: /assets/images/process_scrivania.jpg
    image_path: /assets/images/process_scrivania_th.jpg
    alt: "Scrivania del dottore"
    title: "Scrivania del dottore"
  - url: /assets/images/process_corridoio.jpg
    image_path: /assets/images/process_corridoio_th.jpg
    alt: "Ingresso dello studio"
    title: "Ingresso dello studio"
  - url: /assets/images/process_sala_attesa_3.jpg
    image_path: /assets/images/process_sala_attesa_3_th.jpg
    alt: "Seconda foto della sala d'attesa"
    title: "Seconda foto della sala d'attesa"
---

{% include gallery layout="half" %}
