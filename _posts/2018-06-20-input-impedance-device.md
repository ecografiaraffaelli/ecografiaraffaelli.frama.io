---
title: "input impedance measurement device"
header:
  teaser: /assets/images/banc/teaser-01.jpg
  og_image: /assets/images/banc/teaser-01.jgp
categories:
  - experience
tags:
  - input impedance
gallery:
  - url: /assets/images/banc/1.jpg
    image_path: /assets/images/banc/1.jpg
    alt: "placeholder image 1"
    title: "Image 1 title caption"
  - url: /assets/images/banc/2.jpg
    image_path: /assets/images/banc/2.jpg
    alt: "placeholder image 2"
    title: "Image 2 title caption"
  - url: /assets/images/banc/3.jpg
    image_path: /assets/images/banc/3.jpg
    alt: "placeholder image 3"
    title: "Image 3 title caption"
  - url: /assets/images/banc/4.jpg
    image_path: /assets/images/banc/4.jpg
    alt: "placeholder image 4"
    title: "Image 4 title caption"
  - url: /assets/images/banc/5.jpg
    image_path: /assets/images/banc/5.jpg
    alt: "placeholder image 5"
    title: "Image 5 title caption"
  - url: /assets/images/banc/6.jpg
    image_path: /assets/images/banc/6.jpg
    alt: "placeholder image 6"
    title: "Image 6 title caption"
  - url: /assets/images/banc/7.jpg
    image_path: /assets/images/banc/7.jpg
    alt: "placeholder image 7"
    title: "Image 7 title caption"
  - url: /assets/images/banc/8.jpg
    image_path: /assets/images/banc/8.jpg
    alt: "placeholder image 8"
    title: "Image 8 title caption"
  - url: /assets/images/banc/9.jpg
    image_path: /assets/images/banc/9.jpg
    alt: "placeholder image 9"
    title: "Image 9 title caption"
  - url: /assets/images/banc/10.jpg
    image_path: /assets/images/banc/10.jpg
    alt: "placeholder image 10"
    title: "Image 10 title caption"
  - url: /assets/images/banc/11.jpg
    image_path: /assets/images/banc/11.jpg
    alt: "placeholder image 11"
    title: "Image 11 title caption"
  - url: /assets/images/banc/12.jpg
    image_path: /assets/images/banc/12.jpg
    alt: "placeholder image 12"
---

After 6 months of work, we are happy to say that the first steps in
the making of an input impedance measurement device are completed. The
19th June we undertook the first experimental campaign. We will now
analyze the results and improve our design!

{% include gallery caption="gallery showing various events during the making" %}


<!-- {% include gallery id="gallery2" caption="This is a second gallery example with images hosted externally." %} -->

<!-- {% include gallery id="gallery3" class="full" caption="This is a third gallery example with two images and fills the entire content container." %} -->


<!-- {% include gallery id="gallery" layout="half" caption="This is a half gallery layout example." %} -->
