---
title: "Publications"
permalink: /publications/
layout: single
toc: true
toc_sticky: true
author_profile: true
header:
  image: /assets/images/anaechoiqueBIGToCut.jpg
---

In revision
-------------------

{% bibliography --query @unpublished[keywords=revision] %}
paper submitted to JASA

Submitted
---------

{% bibliography --query @unpublished[keywords=soumis] %}
paper submitted to JASA

Journal articles
----------------

{% bibliography --query @article %}

Ph.D. Thesis
-------------------------
{% bibliography --query @phdthesis %}
